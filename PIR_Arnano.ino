#define PIR_PIN  10    //D10
#define LED_PIN  12      //D12

void setup() { 
  Serial.begin(115200);
  pinMode(PIR_PIN, INPUT);
  pinMode(LED_PIN, OUTPUT);
}
 
void loop() {
int  coming = digitalRead(PIR_PIN);
Serial.println(coming);
 
  if (coming == 1) { 
    digitalWrite(LED_PIN, HIGH);
    delay(3000);
  } else {
    digitalWrite(LED_PIN, LOW);
  }
}


// D13은 특수핀이어서 안됨. 여기서 시간 과다 소비하였음.