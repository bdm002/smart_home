#include "U8glib.h"
#include <SimpleDHT.h>
U8GLIB_SSD1306_128X64 u8g(U8G_I2C_OPT_NONE);
int pinDHT22 = 2; //D2
SimpleDHT22 dht22;

float temperature = 0;
float humidity = 0;

String thisTemp = "";
String thisHumidity="";


void draw() {
u8g.setFont(u8g_font_unifont);
thisTemp = String(temperature) + "C";
const char* newTempC = (const char*) thisTemp.c_str();
u8g.drawStr( 0, 20, newTempC);

u8g.setFont(u8g_font_unifont);
thisHumidity = String(humidity) + "%";
const char* newHumidity = (const char*) thisHumidity.c_str();
u8g.drawStr( 0, 40, newHumidity);

}

void setup() {
  Serial.begin(115200);
}

void loop() {
  Serial.println("================");
  Serial.println("Sample DHT22...");



  int err = SimpleDHTErrSuccess;
  if ((err = dht22.read2(pinDHT22, &temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("Read DHT22 failed, err="); Serial.println(err);delay(2000);
    return;
  }
  Serial.print("Sample OK: ");
  Serial.print((float)temperature); Serial.print(" *C, ");
  Serial.print((float)humidity); Serial.println(" RH%");

  delay(2500);
  
  u8g.firstPage(); 
do {
draw();
} while( u8g.nextPage() );
delay(1000);
}
